﻿using IMDB_api.Context;
using IMDB_api.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace IMDB_api.Repositories
{
    public class UserRepository : IUserRepository
    {

       // private readonly IMDBContext Context;


        public bool Delete(User user)
        {
            IMDBContext.Remove(user);
            return true;
        }

        public List<User> ListAll()
        {
            
            return IMDBContext.Users.ToList();
        }

        public User Save(User user)
        {
            throw new NotImplementedException();
        }

        public User Update(User user)
        {
            throw new NotImplementedException();
        }
    }
}
