﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB_api.Repositories
{
    interface IGeneric<T>
    {
        T Save(T obj);
        bool Delete(T obj);
        T Update(T obj);
        List<T> ListAll();
    }
}
