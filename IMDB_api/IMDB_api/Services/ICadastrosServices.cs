﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB_api.Services
{
    interface ICadastrosServices
    {
        void SaveUser();
        void UpdateUser();
        void DeleteUser();
        void GetUser();


    }
}
