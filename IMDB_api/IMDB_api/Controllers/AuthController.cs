﻿using IMDB_api.Helpers;
using IMDB_api.Model;
using IMDB_api.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB_api.Controllers
{
    [ApiController]
    public class AuthController : ControllerBase
    {
        
        [HttpPost]
        [AllowAnonymous]
        [Route("/api/v1/auth")]
        public async Task<IActionResult> Auth([FromBody] User userRequest)
        {
            try
            {
                if (userRequest == null || userRequest.Email == null || userRequest.Password==null)
                    return BadRequest(new { Message = "Email e/ou senha está(ão) inválido(s)." });


              
               
                return Ok(new
                {
                    Message= "Token "
                });

            }
            catch (Exception)
            {
                return BadRequest(new { Message = "Ocorreu algum erro interno na aplicação, por favor tente novamente." });
            }
        }
    }
}
