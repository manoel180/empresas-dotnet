﻿using IMDB_api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB_api.Controllers
{
    [ApiController]
    [Route("/api/v1/user/")]
    public class UserController : ControllerBase
    {
        ICadastrosServices cadastrosServices = new CadastrosServices();
        [HttpGet]
        //[Authorize]
        public IActionResult GetUsers()
        {
            cadastrosServices.GetUser();
            return Ok("Lista Usuarios");
        }


        [HttpPost]
        //[Authorize(Roles = "Admin")]
        public IActionResult GetManager()
        {
            return Ok("Está logado como gerente: " + User.Identity.Name);
        }


    
    }
}
