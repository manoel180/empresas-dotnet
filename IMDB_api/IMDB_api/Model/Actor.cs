﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB_api.Model
{
    public class Actor
    {
      

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IdActor{ get; set; }
        [Required]
        public string Name { get; set; }
        
        public virtual IList<MovieActor> Movies { get; set; }
    }
}
