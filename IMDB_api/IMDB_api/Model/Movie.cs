﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB_api.Model
{
    public class Movie
    {
       
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IdMovie { get; set; }
        public string Name { get; set; }

         public Director Director { get; set; }

        public virtual IList<MovieActor> Actors { get; set; }

        public virtual IList<MovieGenre> Genre { get; set; }

       
    }
}
