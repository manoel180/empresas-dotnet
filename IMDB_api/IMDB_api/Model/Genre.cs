﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMDB_api.Model
{
    public class Genre
    {
      
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int IdGenre { get; set; }
        [Required]
        public string Name { get; set; }
        [NotMapped]
        public IList<MovieGenre> Movies { get; set; }
    }
}
