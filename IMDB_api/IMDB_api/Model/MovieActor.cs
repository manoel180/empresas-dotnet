﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB_api.Model
{
    public class MovieActor
    {
        public int MovieID { get; set; }
        public Movie Movie { get; set; }
        
        public int ActorID { get; set; }
        public Actor Actor{ get; set; }
    }
}

